﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Collection", menuName = "Image Collection")]
public class SerializedImageCollection : ScriptableObject
{
	[SerializeField]
	public ImageCollection images;
}
