﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.Linq;

namespace FPJT
{
	using FPJT.JSON;

    public class Reporting : MonoBehaviour
	{
		[SerializeField]
		private string path;

		[SerializeField]
		private ShowImage prototype;

		[SerializeField]
		private int howMany;

		private IEnumerable<Utils.FSEntry> fSEntries;

		private void Start()
		{
			fSEntries = Utils.LoadEntries(path);
			fSEntries = groupByIndex(fSEntries);
			fSEntries = fSEntries.OrderByDescending(entry => entry.Clicked).ThenBy(entry => entry.Rank);
			prototype.gameObject.SetActive(false);
			show();
		}

		private void show()
		{
			int ctr = 1;
			foreach(var candidate in fSEntries.Take(howMany))
			{
				ShowImage image = Instantiate(prototype.gameObject, prototype.transform.position,
					prototype.transform.rotation, prototype.transform.parent).GetComponent<ShowImage>();
				//Color col = image.ImageRenderer.color;
				//col.a = 1f / howMany;
				//image.ImageRenderer.color = col;
				image.ID = candidate.Index;
				image.gameObject.SetActive(true);
				image.UpdateImage();

				Debug.Log($"{ctr++}) Clicks: {candidate.Clicked}, Rank: {candidate.Rank}, Unique: {candidate.TimesRanked}");
			}
		}

		private IEnumerable<Utils.FSEntry> groupByIndex(IEnumerable<Utils.FSEntry> entries)
		{
			Dictionary<int, Utils.FSEntry> rankedEntries = new Dictionary<int, Utils.FSEntry>();

			foreach(var entry in entries)
			{
				if (rankedEntries.TryGetValue(entry.Index, out Utils.FSEntry rankedEntry))
				{
					rankedEntries[entry.Index] = Add(entry, rankedEntry);
				}
				else
				{
					rankedEntries.Add(entry.Index, entry);
				}
			}

			return rankedEntries.Values;
		}

		private Utils.FSEntry Add(Utils.FSEntry lhs, Utils.FSEntry rhs)
		{
			Utils.FSEntry result = null;
			if (lhs.Index == rhs.Index)
			{
				float lhsExpanded = lhs.Rank * lhs.TimesRanked;
				float rhsExpanded = rhs.Rank * rhs.TimesRanked;
				float rank = (lhsExpanded + rhsExpanded) / (lhs.TimesRanked + rhs.TimesRanked);
				result = new Utils.FSEntry()
				{
					Rank = rank,
					TimesRanked = lhs.TimesRanked + rhs.TimesRanked,
					Index = lhs.Index,
					Path = lhs.Path + rhs.Path,
					Seen = lhs.Seen + rhs.Seen,
					Clicked = lhs.Clicked + rhs.Clicked,
				};
			}

			return result;
		}
	}
}
