﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DemographicForm : MonoBehaviour
{
	[SerializeField]
	private Button continueButton;

	private void Start()
	{
		activateButton(continueButton, formFilled());
	}

	public void OnNameInput(string name)
	{
		DemographicInfo.Username = name;
		activateButton(continueButton, formFilled());
	}

	public void OnAgeInput(string age)
	{
		DemographicInfo.Age = age;
		activateButton(continueButton, formFilled());
	}

	public void OnGenderSelect(bool isFemale)
	{
		DemographicInfo.Gender = isFemale ? "female" : "male";
		activateButton(continueButton, formFilled());
	}

	public void OnContinue()
	{
		if (formFilled())
		{
			SceneManager.LoadScene(1);
		}
	}

	private bool formFilled()
	{
		return !string.IsNullOrEmpty(DemographicInfo.Age)
				&& !string.IsNullOrEmpty(DemographicInfo.Username)
				&& !string.IsNullOrEmpty(DemographicInfo.Gender);
	}

	private void activateButton(Button button, bool value)
	{
		button.interactable = value;
	}
}
