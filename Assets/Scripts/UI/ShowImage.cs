﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowImage : MonoBehaviour
{
    public int ID;

    [SerializeField]
    private SerializedImageCollection collection;

    public Image ImageRenderer;

    public void UpdateImage()
	{
        if (ImageRenderer != null
            && collection != null)
        {
            ImageRenderer.sprite = collection.images[ID];
        }
    }

    private void OnValidate()
    {
        UpdateImage();
    }
}
