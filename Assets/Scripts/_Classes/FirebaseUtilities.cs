﻿using System.Runtime.InteropServices;

public static class FirebaseUtilities
{
	[DllImport("__Internal")]
	public static extern void Hello();

	[DllImport("__Internal")]
	private static extern void ImageClicked(string str, string name, int age, string gender);

	[DllImport("__Internal")]
	private static extern void ImageSeen(string str, string name, int age, string gender);

	[DllImport("__Internal")]
	private static extern void ImageRanked(string str, int rank, string name, int age, string gender);

	[DllImport("__Internal")]
	private static extern void Log(string msg);

	private static int age => int.Parse(DemographicInfo.Age);

	public static void ClickImage(int id) => ImageClicked(id.ToString(), DemographicInfo.Username, age, DemographicInfo.Gender);

	public static void SeenImage(int id) => ImageSeen(id.ToString(), DemographicInfo.Username, age, DemographicInfo.Gender);

	public static void RankImage(int id, int rank) => ImageRanked(id.ToString(), rank, DemographicInfo.Username, age, DemographicInfo.Gender);
}
