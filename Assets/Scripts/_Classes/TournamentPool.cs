﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TournamentPool : MonoBehaviour
{
	[SerializeField]
	private SerializedImageCollection imageCollection;

	[SerializeField]
	private int poolSize;

	List<int> keysPool = new List<int>();

	[SerializeField]
	private Image contestantA;

	[SerializeField]
	private Image contestantB;

	[SerializeField]
	List<int> knockoutPool = new List<int>();

	[SerializeField]
	private int seedIndexA = 0;
	[SerializeField]
	private int seedIndexB = 1;

	private Image lastRoundVictor;
	private int lastRoundVictorKey;

	private int currentRank;

	void Awake()
    {
		int count = imageCollection.images.Count;

		List<int> keys = new List<int>();

		while (keys.Count < poolSize){
			int key = Random.Range(0, count);
			if (!keys.Contains(key)) {
				keys.Add(key);
			}
		}

		currentRank = poolSize;

		keysPool = keys;

		contestantA.sprite = imageCollection.images[keysPool[seedIndexA]];
		contestantB.sprite = imageCollection.images[keysPool[seedIndexB]];
		FirebaseUtilities.SeenImage(keysPool[seedIndexA]);
		FirebaseUtilities.SeenImage(keysPool[seedIndexB]);
	}


    public void OnASelected(){
		knockoutPool.Add(seedIndexB);
		lastRoundVictor = contestantA;
		lastRoundVictorKey = keysPool[seedIndexA];
		FirebaseUtilities.ClickImage(keysPool[seedIndexA]);
		FirebaseUtilities.RankImage(keysPool[seedIndexB], currentRank--);
		BumpMatch();
	}

	public void OnBSelected() {
		knockoutPool.Add(seedIndexA);
		lastRoundVictor = contestantB;
		lastRoundVictorKey = keysPool[seedIndexB];
		FirebaseUtilities.ClickImage(keysPool[seedIndexB]);
		FirebaseUtilities.RankImage(keysPool[seedIndexA], currentRank--);
		BumpMatch();
	}

	private void BumpMatch() {
		seedIndexA+=2;
		seedIndexB+=2;

		if(seedIndexB >= keysPool.Count) {
			BumpRound();
		} else {
			contestantA.sprite = imageCollection.images[keysPool[seedIndexA]];
			contestantB.sprite = imageCollection.images[keysPool[seedIndexB]];
			FirebaseUtilities.SeenImage(keysPool[seedIndexA]);
			FirebaseUtilities.SeenImage(keysPool[seedIndexB]);
		}
	}

	private void BumpRound() {
		Debug.Log("NEW ROUND");
		int bottomRank = poolSize;
		//foreach (int index in knockoutPool) {
		//	//int id = keysPool[index];
		//	//Rank
		//	//imageCollection.images[keysPool[id]]
		//	Debug.Log(index);
		//	keysPool.RemoveAt(index + buffer);
		//	buffer++;
		//}
		List<int> nextRoundPool = new List<int>();
		for(int i = 0; i < keysPool.Count; i++) {
			if (!knockoutPool.Contains(i)) {
				nextRoundPool.Add(keysPool[i]);
			}
		}
		keysPool = nextRoundPool;
		knockoutPool.Clear();
		seedIndexA = 0;
		seedIndexB = 1;

		if(keysPool.Count > 1) {
			contestantA.sprite = imageCollection.images[keysPool[seedIndexA]];
			contestantB.sprite = imageCollection.images[keysPool[seedIndexB]];
		} else {
			Debug.Log("WINNER FOUND");
			contestantA.GetComponent<Button>().enabled = false;
			contestantB.GetComponent<Button>().enabled = false;
			FirebaseUtilities.RankImage(lastRoundVictorKey, 1);
			if (contestantA == lastRoundVictor) {
				Destroy(contestantB.gameObject);
			} else {
				Destroy(contestantA.gameObject);
			}
		}

	}
}
