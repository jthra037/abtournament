﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web;
using Newtonsoft.Json;

namespace FPJT.JSON
{
	public static class Utils
	{
		public class FSEntry
		{
			public int TimesRanked;
			public int Seen;
			public int Clicked;
			public float Rank;
			public string Path;
			public int Index;
		}

		public static IEnumerable<FSEntry> LoadEntries(string path)
		{
			List<FSEntry> entryList = new List<FSEntry>();
			string line;

			using (StreamReader reader = new StreamReader(path))
			{
				while ((line = reader.ReadLine()) != null)
				{
					entryList.Add(JsonConvert.DeserializeObject<FSEntry>(line));
				}
			}

			return entryList;
		}
	}
}