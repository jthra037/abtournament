﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NSJT.Collections
{
	[Serializable]
	public class Map<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>, IEnumerable, ICollection<TValue>, IEnumerable<TValue>, ISerializationCallbackReceiver
	{
		[SerializeReference]
		protected TValue[] _values;

		private Dictionary<TKey, TValue> map;
		private readonly Func<TValue, TKey> keygen;

		public int Count => ((ICollection<KeyValuePair<TKey, TValue>>)map).Count;

		public bool IsReadOnly => ((ICollection<KeyValuePair<TKey, TValue>>)map).IsReadOnly;

		public TValue this[TKey key]
		{
			get
			{
				return map[key];
			}
		}

		public Map()
		{
			map = new Dictionary<TKey, TValue>();
		}

		public Map(Func<TValue, TKey> keygen) : this()
		{
			this.keygen = keygen;
		}

		public Map(Func<TValue, TKey> keygen, IEnumerable<TValue> values) : this(keygen)
		{
			foreach (TValue value in values)
			{
				Add(value);
			}
		}

		public virtual TKey GetKey(TValue value)
		{
			return keygen(value);
		}

		public bool ContainsKey(TKey key)
		{
			return map.ContainsKey(key);
		}

		public bool TryGetValue(TKey key, out TValue value)
		{
			return map.TryGetValue(key, out value);
		}

		#region Interface Implementations
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			return ((IEnumerable<KeyValuePair<TKey, TValue>>)map).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<KeyValuePair<TKey, TValue>>)map).GetEnumerator();
		}

		public void Add(TValue value)
		{
			map.Add(GetKey(value), value);
		}

		public void Clear()
		{
			map.Clear();
		}

		public bool Contains(TValue item)
		{
			return map.Values.Contains(item);
		}

		public void CopyTo(TValue[] array, int index)
		{
			map.Values.CopyTo(array, index);
		}

		public bool Remove(TValue item)
		{
			return map.Remove(GetKey(item));
		}

		IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
		{
			return map.Values.GetEnumerator();
		}

		public void OnBeforeSerialize() { }

		public void OnAfterDeserialize()
		{
			for (int i = 0; i < _values.Length; i++)
			{
				if (!map.ContainsKey(GetKey(_values[i])))
				{
					Add(_values[i]);
				}
			}
		}
		#endregion
	}
}