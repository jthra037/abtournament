﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace NSJT.Collections
{
	[Serializable]
	public class IntMap<TValue> : Map<int, TValue>
	{
		public IntMap() : base()
		{
		}

		public IntMap(Func<TValue, int> keygen) : base(keygen)
		{
		}
	}
}