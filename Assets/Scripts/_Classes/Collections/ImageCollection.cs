﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSJT.Collections;
using UnityEngine;

[Serializable]
public class ImageCollection : IntMap<Sprite>
{
	public override int GetKey(Sprite value)
	{
		return Array.FindIndex(_values, wanted => value.Equals(wanted));
	}
}
