mergeInto(LibraryManager.library, {
  Hello: function () {
    window.alert("Hello, world!");
  },
  //
  //HelloString: function (str) {
  //  window.alert(Pointer_stringify(str));
  //},
  //
  //PrintFloatArray: function (array, size) {
  //  for (var i = 0; i < size; i++)
  //    console.log(HEAPF32[(array >> 2) + i]);
  //},
  //
  //AddNumbers: function (x, y) {
  //  return x + y;
  //},
  //
  //StringReturnValueFunction: function () {
  //  var returnStr = "bla";
  //  var bufferSize = lengthBytesUTF8(returnStr) + 1;
  //  var buffer = _malloc(bufferSize);
  //  stringToUTF8(returnStr, buffer, bufferSize);
  //  return buffer;
  //},
  //
  //BindWebGLTexture: function (texture) {
  //  GLctx.bindTexture(GLctx.TEXTURE_2D, GL.textures[texture]);
  //},
  Log : function(msg)
  {
    console.log(Pointer_stringify(msg));
  },

  ImageClicked : ['db', 'firebase'],
  ImageClicked : function(id, name, newAge, gender) {
	var personRef = db.collection(Pointer_stringify(gender))
				      .doc(Pointer_stringify(name));

    var docRef = personRef.collection('images')
	               .doc(Pointer_stringify(id));
				   
	personRef.set({age : newAge}, {merge: true});

    db.runTransaction(function(transaction) {
        // This code may get re-run multiple times if there are conflicts.
        return transaction.get(docRef).then(function(imageDoc) {
            var newClicked;
			
			if (!imageDoc.exists) {
                newClicked = 1;
            }
			else
			{
			  newClicked = imageDoc.data().clicked + 1;
			  if (isNaN(newClicked))
			  {
			    newClicked = 1;
			  }
			}
			
            transaction.set(docRef, {clicked: newClicked}, {merge: true});
        });
    }).then(function() {
        console.log("Transaction successfully committed!");
    }).catch(function(error) {
        console.log("Transaction failed: ", error);
    });
  },

  ImageSeen : ['db', 'firestore'],
  ImageSeen : function(id, name, newAge, gender) {
	var personRef = db.collection(Pointer_stringify(gender))
					  .doc(Pointer_stringify(name));

    var docRef = personRef.collection('images')
	               .doc(Pointer_stringify(id));
				   
	personRef.set({age : newAge}, {merge: true});

    db.runTransaction(function(transaction) {
        // This code may get re-run multiple times if there are conflicts.
        return transaction.get(docRef).then(function(imageDoc) {
            var newSeen;
			
			if (!imageDoc.exists) {
                newSeen = 1;
            }
			else
			{
			  newSeen = imageDoc.data().seen + 1;
			  if (isNaN(newSeen))
			  {
			    newSeen = 1;
			  }
			}
			
            transaction.set(docRef, {seen: newSeen}, {merge: true});
        });
    }).then(function() {
        console.log("Transaction successfully committed!");
    }).catch(function(error) {
        console.log("Transaction failed: ", error);
    });
  },

  ImageRanked : ['db', 'firestore'],
  ImageRanked : function(id, rank, name, newAge, gender) {
	var personRef = db.collection(Pointer_stringify(gender))
					  .doc(Pointer_stringify(name));

    var docRef = personRef.collection('images')
	               .doc(Pointer_stringify(id));
				   
	personRef.set({age : newAge}, {merge: true});

    db.runTransaction(function(transaction) {
        // This code may get re-run multiple times if there are conflicts.
        return transaction.get(docRef).then(function(imageDoc) {
            var newRank;
			var newTimesRanked;
			
			if (!imageDoc.exists) 
			{
              newTimesRanked = 1;
			  newRank = rank;
            }
			else
			{
			  var oldRank = imageDoc.data().rank;
			  var oldTimesRanked = imageDoc.data().timesRanked;
			  newTimesRanked = imageDoc.data().timesRanked + 1;
			  // no null coallescing operator I guess?
			  if (isNaN(newTimesRanked))
			  {
				  newTimesRanked = 1;
			  }
			  newRank = ((oldRank * oldTimesRanked) + rank) / newTimesRanked;
			  if (isNaN(newRank))
			  {
				  newRank = rank;
			  }
			}
			
            transaction.set(docRef, {rank: newRank, timesRanked: newTimesRanked}, {merge: true});
        });
    }).then(function() {
        console.log("Transaction successfully committed!");
    }).catch(function(error) {
        console.log("Transaction failed: ", error);
    });
  },
});